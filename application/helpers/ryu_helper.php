<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
function tableList(){
    return array(
        'tr11_penerimaan_tidak_terikat_pending',
        'tr12_penerimaan_terikat_pending',
        'tr21_pembelian_pending',
        'tr22_beban_pending',
        'tr23_renov_bangun_pending'
    );
}